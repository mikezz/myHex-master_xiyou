// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html
cc.Class({
    extends: cc.Component,
    properties: {
        m_popLabel:cc.Label,
    },
    // LIFE-CYCLE CALLBACKS:
    onLoad () {

    },
    //
    start () {

    },
    //
    popMsg( msg ) {
        this.m_popLabel.string = msg;
        let animation = this.m_popLabel.getComponent(cc.Animation);
        animation.stop();
        animation.play();
    },
    
    // update (dt) {},
});
