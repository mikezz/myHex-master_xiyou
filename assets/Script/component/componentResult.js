// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,
    properties: {
        m_scoreNode:cc.Node,
    },
    // LIFE-CYCLE CALLBACKS:
    onLoad () {
        this.m_score = 0;
    },
    
    //
    start () {

    },
    //
    showScore( score ){
        this.m_score = score;
        this.node.active = true;
        let componentScrollNumLabel = this.m_scoreNode.getComponent('componentScrollNumLabel');
        componentScrollNumLabel.scrollToNum(0, score, 0.1);
        cc.log('显示结果');
        window.Helper.wx.setSubNodeVisible(true);
        window.Helper.wx.postMsg('result', {type:'score', is_need_submit:true,value:score});
    },
    //
    onBtnClose(){

    },

    onBtnShare(){
        let msg = '您的好友已经在西游消消乐中获得' + this.m_score + '分,快去和他一起营救师傅吧';
        window.Helper.wx.share(msg);
    }
    // update (dt) {},
});
