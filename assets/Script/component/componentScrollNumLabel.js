// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,
    properties: {
        m_totalNum:0,
        m_curNum:0,
        m_speed:0,
        m_label:{
            type:cc.Label,
            default:null
        },
    },
    // LIFE-CYCLE CALLBACKS:
    // onLoad () {},
    start () {

    },
    scrollToNum(cur_num, total_num, speed){
        this.m_totalNum = total_num;
        this.m_curNum = cur_num;
        this.m_speed = (total_num - cur_num)*speed;
        this.m_isStart = true;
    },
    updateNum(){
        this.m_curNum += this.m_speed;
        this.m_label.string = Math.floor(this.m_curNum);
    },
    checkIsStop(){
        if ( this.m_curNum >= this.m_totalNum ){
            this.m_isStart = false;
            return true;
        }
        return false;
    },
    update (dt) {
        if (this.m_isStart){
            this.updateNum();
            this.checkIsStop();
        }
    },
});
