// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,
    properties: {
        rankingScrollView: cc.Sprite,//显示排行榜
    },
    // LIFE-CYCLE CALLBACKS:
    onLoad () {
        this.m_isDealyUpdate = false;
        this.m_delayTime = 0.5;
        if (window.wx != undefined) {
            this.login();
            this.tex = new cc.Texture2D();
            var sharedCanvas = window.sharedCanvas
            if (sharedCanvas) {
                sharedCanvas.width = cc.game.canvas.width * 2;
                sharedCanvas.height = cc.game.canvas.height * 2;
            }
            window.Helper.wx.initAD('home','adunit-5187e91365899395');
        }
        window.Helper.wx.setSubNode(this.node);
        window.Helper.wx.setSubNodeVisible(false);
    },
    start () {

    },

    // 刷新子域的纹理
    _updateSubDomainCanvas() {
        if (window.sharedCanvas != undefined) {
            this.tex.initWithElement(window.sharedCanvas);
            this.tex.handleLoadedTexture();
            this.rankingScrollView.spriteFrame = new cc.SpriteFrame(this.tex);
        }
    },

     //设置延时刷新
     setDelayUpdate( isDelay, delayTime ){
        console.log('设置延时刷新',isDelay);
        if(delayTime){
            this.m_delayTime = delayTime;
        }
        this.m_isDealyUpdate = isDelay;
        if(!isDelay){
            this.m_cd = 0;
        }
    },
    
    //
    update( dt ) {
        if(this.m_isDealyUpdate){
            if(this.m_cd>0){
                this.m_cd -= dt;
                return;
            }
        }
        this._updateSubDomainCanvas();
        if(this.m_isDealyUpdate){
            this.m_cd = this.m_delayTime;
        }
    },
       
    //
    login(){
        console.log('开始登陆...');
        let self = this;
        wx.login({
            success: function(){
                console.log('登录成功...');
                //获取用户信息
                wx.getUserInfo({
                    success: function(res){
                        if(window.wx){
                            window.wx.postMessage({
                                msg:'data',
                                params:{value: 'getSelfUserData',type:'score'},
                            });
                            window.wx.postMessage({
                                msg:'data',
                                params:{value: 'getFriendRankData',type:'score'},
                            });
                        }
                    },
                    fail: function(){
                        console.log('用户信息获取失败...');
                    }
                })
            },
            fail: function(){
                console.log("登录失败...")
            }
        });
    },
    
});
