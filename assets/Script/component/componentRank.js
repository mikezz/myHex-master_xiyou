// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,
    properties: {

    },

    // LIFE-CYCLE CALLBACKS:
    onLoad () {
       
    },

    //
    showRank(){
        this.node.active = true;
        window.Helper.wx.postMsg('rank', {type:'score'});
        window.Helper.wx.setSubNodeVisible(true);
    },

    //
    onBtnClose(){
        window.Helper.wx.postMsg('hide', {ui:'rank'});
        window.Helper.wx.setSubNodeVisible(false);
        this.node.active = false;
    }
    // update (dt) {},
});
