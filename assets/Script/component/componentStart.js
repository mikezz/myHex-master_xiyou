// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,
    properties: {
        m_rankNode:cc.Node,
        m_battleNode:cc.Node,
    },
    // LIFE-CYCLE CALLBACKS:
    onLoad () {
        this.node.active = true;
    },
    //
    start () {
        window.Helper.wx.setSubNodeVisible(false);
    },
    //
    onBtnRank(){
        let componentRank = this.m_rankNode.getComponent('componentRank');
        componentRank.showRank();
    },
    //
    onBtnShare(){
        window.Helper.wx.share();
    },
    //
    onBtnStartGame(){
        this.node.active = false;
        let battle = this.m_battleNode.getComponent('Battle');
        battle.gameStart();
    }
    // update (dt) {},
});
