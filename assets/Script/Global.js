// 全局数据中心
if (window == undefined){
    window = {};
}
//全局帮助类
window.Helper = {};
//声音管理
var MusicHelper = require('./util/MusicHelper');
window.Helper.music = new MusicHelper();
//动作管理
var ActionHelper = require('./util/ActionHelper');
window.Helper.action = new ActionHelper();
//动作管理
var MathHelper = require('./util/MathHelper');
window.Helper.math = new MathHelper();
//微信管理
var WxHelper = require('./util/WXHelper');
window.Helper.wx = new WxHelper();


