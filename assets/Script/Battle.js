// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,
    properties: {
        //师傅节点
        m_shifuNode:cc.Node,
        //时间
        m_cdLabel:cc.Label,
        //最大位移
        m_totalX:500,
        //总的时间
        m_totalTime:30,
        //剩余的时间
        m_leftTime:30,
        //换算百分比
        m_timePercent:1,
        //当前的等级
        m_curLevel:1,
        //
        m_operateNode:cc.Node,
        //
        m_boardNode:cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.m_timeCD = 1;
        this.m_gameStart = false;
    },

    start () {

    },

    update (dt) {
        if(!this.m_gameStart){
            return;
        }
        if(this.m_timeCD){
            this.m_timeCD-=dt
            if(this.m_timeCD<=0){
                this.updateTime(-1);
                this.m_timeCD = 1;
            }
        }
    },

    updateTime( time ){
        if(time>0){
            time = Math.floor(this.m_timePercent * time);
        }
        this.m_leftTime += time;
        this.m_leftTime = Math.min( this.m_leftTime, this.m_totalTime);
        this.m_cdLabel.string = this.m_leftTime;
        this.m_shifuNode.x = ( 1 - this.m_leftTime/this.m_totalTime - 0.5) * this.m_totalX;
        if(this.m_leftTime<=0){
            this.gameOver();
        }
    },

    gameOver(){
        cc.log('游戏结束.....');
        this.m_gameStart = false;
        this.m_boardNode.getComponent('Board').gameOver();
    },

    gameStart(){
        this.m_gameStart = true;
    }
});
