/* ---------------------------------------------------------------
 *  数学帮助类
 * ---------------------------------------------------------------
 */
let MathHelper = cc.Class({
    getRad: function( start,  end ){
        let offset_pos = cc.pSub( end, start );
        let angle = cc.pToAngle ( offset_pos );
        if( offset_pos.y < 0 ) {
            angle += 2 * Math.PI;
        }
        //cc.log('pos:', offset_pos,'rad', angle,'angle:', angle * 180 / Math.PI );
        return angle;
    },

    ///获取角度
    getAngle: function ( start,  end ){
        let rad = this.getRad( start, end );
        return rad * 180 / Math.PI;
    },
    
    //取随机数
    getRandom: function ( Min, Max, isInt){
        var Range = Max - Min;   
        var Rand = Math.random();
        let number = (Min + Math.round(Rand * Range));
        if( isInt ) {
            return Math.floor(number);    
        }    
        return number;  
    },

    //获取 圆弧上的 随机落点
    getRadiusRandomPos: function( a, b ){
         var radius = cc.random0To1() * 360 * 0.017453293;
         var sinx = Math.sin( radius );
         var cosx = Math.cos( radius );
         var x = b * sinx ;
         var y = a * cosx ;
         var radius_pos = cc.p (x, y );
         var pos = cc.pMult(radius_pos, cc.random0To1());
         return pos;
    },

    //判断矩形是否碰撞
    checkIsRectContactRect: function( rect1, rect2 ) {
        if( rect1.intersects(rect2) || cc.Rect.contain(rect1, rect2) != 0 ){
            return true;
        }
        return false;
    },

    //检测是否圆和矩形相交
    checkIsCircleContactRect: function(circle_center, radius, rect_height, rect_width, rect_center ){
        let h = cc.p( rect_width, rect_height) ;
        let offset = cc.p( Math.abs(circle_center.x - rect_center.x), Math.abs(circle_center.y - rect_center.y) );
        let h_pos = cc.p( Math.max(offset.x - h.x, 0) , Math.max(offset.y - h.y, 0) ); 
        let dis = cc.pLength( h_pos );
        //cc.log('circle_center',circle_center, 'radius', radius, 'rect_height', rect_height, 'rect_width', rect_width, 'rect_center', rect_center);
        //cc.log('h',h, 'offset', offset, 'h_pos', h_pos, 'dis', dis );
        let isContact = dis <= radius;
        return isContact;
    },

    //检测 直线 是否和矩形相交
    checkIsLineContactRect: function( rect, start_pos, end_pos )  {
        let is_start_in = rect.contains( start_pos );
        let is_end_in = rect.contains( end_pos );
        let max_x = rect.xMax;
        let min_x = rect.xMin;
        let max_y = rect.yMax;
        let min_y = rect.yMin;
        if( (is_start_in && is_end_in)|| is_start_in ) {
            //cc.log('is_start_in....', rect);
            return cc.p(min_x - 1, min_y - 1);
        }

        let p1 = cc.p(min_x, min_y);
        let p2 = cc.p(min_x, max_y);
        let p3 = cc.p(max_x, min_y);
        let p4 = cc.p(max_x, max_y);
        if( is_end_in ) {
            let pos = null;
            pos = this.segmentsIntr(start_pos, end_pos, p1, p2);
            if(pos) {
                pos.x = pos.x -1;
                return pos;
            }
            pos = this.segmentsIntr(start_pos, end_pos, p1, p3);
            if(pos) {
                pos.y = pos.y -1;
                return pos;
            }
            pos = this.segmentsIntr(start_pos, end_pos, p4, p2);
            if(pos) {
                pos.y = pos.y +1;
                return pos;
            }
            pos = this.segmentsIntr(start_pos, end_pos, p4, p3);
            if(pos) {
                pos.x = pos.x +1;
                return pos;
            }
        }
        return false;
    },

    segmentsIntr: function(a, b, c, d) {  
        // 三角形abc 面积的2倍  
        var area_abc = (a.x - c.x) * (b.y - c.y) - (a.y - c.y) * (b.x - c.x);  
    
        // 三角形abd 面积的2倍  
        var area_abd = (a.x - d.x) * (b.y - d.y) - (a.y - d.y) * (b.x - d.x);   
    
        // 面积符号相同则两点在线段同侧,不相交 (对点在线段上的情况,本例当作不相交处理);  
        if ( area_abc*area_abd>=0 ) {  
            return false;  
        }  
    
        // 三角形cda 面积的2倍  
        var area_cda = (c.x - a.x) * (d.y - a.y) - (c.y - a.y) * (d.x - a.x);  
        // 三角形cdb 面积的2倍  
        // 注意: 这里有一个小优化.不需要再用公式计算面积,而是通过已知的三个面积加减得出.  
        var area_cdb = area_cda + area_abc - area_abd ;  
        if (  area_cda * area_cdb >= 0 ) {  
            return false;  
        }  
    
        //计算交点坐标  
        var t = area_cda / ( area_abd- area_abc );  
        var dx= t * (b.x - a.x),  
            dy= t * (b.y - a.y);  
        return { x: a.x + dx , y: a.y + dy };    
    }, 

    ///是否在范围内 1目标的位置 2目标的大小 3当前的位置 4半径范围 （圆与矩形相交）
    isInRectRange: function( target_pos, target_rect, cur_pos, radius, isPoint ) {
        let size = target_rect.size; 
        let center = target_rect.center;
        let dis = cc.pDistance( target_pos, cur_pos );
        let isContact = false;
        if( cc.sizeEqualToSize ( size,  cc.size.ZERO ) || isPoint ) {
            isContact = dis <= radius;
        } else {
            isContact = window.math.checkIsCircleContactRect(cur_pos, radius, size.height, size.width, center);
        }
        return isContact;
    },

    ///计算角度
    calcRotation: function( old_pos, new_pos ) {
        let rotation = 0;
        if(cc.pointEqualToPoint(old_pos, new_pos)){
            return rotation;
        }
        let radio = Math.atan( Math.abs(new_pos.y - old_pos.y) / Math.abs(new_pos.x - old_pos.x) );
        rotation = radio * 180 / Math.PI;
        if (new_pos.x >= old_pos.x && new_pos.y >= old_pos.y) {
            rotation = 360.0 - rotation;
        }
        else if (new_pos.x <= old_pos.x && new_pos.y >= old_pos.y) {
            rotation = 180.0 + rotation;
        }
        else if (new_pos.x <= old_pos.x && new_pos.y <= old_pos.y) {
            rotation = 180.0 - rotation;
        }
        return rotation;
    },

     /*
    伤害计算
        攻击 = 攻击方攻击 * 攻击加成百分比 + 攻击加成数值
        防御 = 防守方防御 * 防御加成百分比 + 防御加成数值
        浮动因子 = 0.95~1.05之间随机
        伤害 =（（攻击-防御）* 技能系数a + 技能系数b）* 浮动因子 
    最低伤害
        若伤害低于最低伤害，则使用最低伤害
        最低伤害 = floor（攻击*0.05,1）
        若最低伤害 ＜ 1，则取 1
        加上技能的判断
    */
    /// 计算最终伤害值
    caculateDamage: function ( attack, defence, coea, coeb ){
       let damage_percent = 1.0 + 0.05 - cc.random0To1() * 0.1;
       let damage = (( attack - defence ) * coea + coeb) * damage_percent;
       damage = Math.floor( damage );
       if ( damage < 1 ){
           damage = 1 ;
       }
       return damage;
    },

    ///计算百分比血量
    caculatePercentHp: function ( maxHp, percent ){
       let hp = maxHp * percent;
       return hp;
    },
    segmentsIntr: function(a, b, c, d){  
  
        // 三角形abc 面积的2倍  
        var area_abc = (a.x - c.x) * (b.y - c.y) - (a.y - c.y) * (b.x - c.x);  
    
        // 三角形abd 面积的2倍  
        var area_abd = (a.x - d.x) * (b.y - d.y) - (a.y - d.y) * (b.x - d.x);   
    
        // 面积符号相同则两点在线段同侧,不相交 (对点在线段上的情况,本例当作不相交处理);  
        if ( area_abc*area_abd>=0 ) {  
            return false;  
        }  
    
        // 三角形cda 面积的2倍  
        var area_cda = (c.x - a.x) * (d.y - a.y) - (c.y - a.y) * (d.x - a.x);  
        // 三角形cdb 面积的2倍  
        // 注意: 这里有一个小优化.不需要再用公式计算面积,而是通过已知的三个面积加减得出.  
        var area_cdb = area_cda + area_abc - area_abd ;  
        if (  area_cda * area_cdb >= 0 ) {  
            return false;  
        }  
    
        //计算交点坐标  
        var t = area_cda / ( area_abd- area_abc );  
        var dx= t*(b.x - a.x),  
            dy= t*(b.y - a.y);  
        return cc.v2(a.x + dx, a.y + dy);  
    }  
    
});


module.exports = MathHelper;