/* ---------------------------------------------------------------
 *  音乐帮助类
 * ---------------------------------------------------------------
 */
let ActionHelper = cc.Class({
    //抖动特效
    CreateScaleShakeAction(time, dis, target_scale){
        let scale = target_scale;
        let action = cc.sequence(cc.scaleTo(time, scale.x+dis, scale.y-dis),
            cc.scaleTo(time, scale.x, scale.y),
            cc.scaleTo(time, scale.x + 0.5 * dis , scale.y- 0.5 * dis),
            cc.scaleTo(time, scale.x, scale.y)
        );
        return action;
    }
});
module.exports = ActionHelper;