/* ---------------------------------------------------------------
 *  音乐帮助类
 * ---------------------------------------------------------------
 */
let WXHelper = cc.Class({
    //创建头像
    createHeadImage(avatarUrl, sprite) {
        if (window.wx != undefined) {
            try {
                let image = wx.createImage();
                image.onload = () => {
                    try {
                        let texture = new cc.Texture2D();
                        texture.initWithElement(image);
                        texture.handleLoadedTexture();
                        sprite.spriteFrame = new cc.SpriteFrame(texture);
                    } catch (e) {
                        cc.log(e);
                        sprite.node.active = false;
                    }
                };
                image.src = avatarUrl;
            }catch (e) {
                cc.log(e);
                sprite.node.active = false;
            }
        } else {
            cc.loader.load({
                url: avatarUrl, type: 'jpg'
            }, (err, texture) => {
                sprite.spriteFrame = new cc.SpriteFrame(texture);
            });
        }
    },
    //
    initAD(type,id){
        if(window.wx == undefined){
            return;
        }
        if(!this.m_adMaps){
            this.m_adMaps = {};
        }
        //
        let ad = this.m_adMaps[type];
        if(ad){
            ad.destroy();
        }
        ad = this.createAd(id);
        this.m_adMaps[type] = ad;
        for(var key in this.m_adMaps){
            if(key != type){
                this.m_adMaps[key].hide();
            }
        }
    },
    //
    createAd(id){
        let systemInfo = wx.getSystemInfoSync();
        let width = systemInfo.windowWidth * 0.95;
        let bannerAd = wx.createBannerAd({
            adUnitId: id,style: {
                left: 0,
                top: 0,
                width: width
            }
        });
        bannerAd.onResize(res => {
            bannerAd.style.left =  (systemInfo.windowWidth - bannerAd.style.realWidth)/2;
            bannerAd.style.top = 0;
            //bannerAd.style.top =  systemInfo.windowHeight - bannerAd.style.realHeight;
          });
        bannerAd.show(); 
        return bannerAd;
    },
    //
    hideAdAll(){
        for(var key in this.m_adMaps){
            this.m_adMaps[key].hide();
        }
    },
    //
    share(msg, success_callfunc, fail_callfunc ){
        if (window.wx){
            let self = this;
            if(!msg){
                let msg_map = [];
                msg_map.push("大师兄师傅被抓走啦!");
                msg_map.push("这位客官你要的是椒盐唐僧,油爆唐僧,还是红烧唐僧!");
                msg_map.push("这位骚年我看你骨骼惊奇不如随我一同去救师傅!");
                let index = Math.floor(Math.random()*msg_map.length);
                index = Math.min(index,msg_map.length-1);
                msg = msg_map[index];
            }
            cc.loader.loadRes('share/share1', cc.Texture2D, function (err, tex) {
                window.wx.shareAppMessage({
                    title: msg,
                    imageUrl:tex.url,
                    success: function(res){
                        console.log('分享成功...');
                        if(success_callfunc){
                            success_callfunc();
                        }
                    },
                    fail: function(){
                        console.log('分享失败...');
                        if(fail_callfunc){
                            fail_callfunc();
                        }
                    }
                });
              });
        }
    },
    //
    postMsg(msg, params){
        if(window.wx){
            window.wx.postMessage({
                msg: msg,
                params:params,
            });
        }
    },
    //
    setSubNode( node ){
        this.m_subNode = node;
    },
    //
    setSubNodeVisible( isShow ){
        this.m_subNode.active = isShow;
    }
});

module.exports = WXHelper;