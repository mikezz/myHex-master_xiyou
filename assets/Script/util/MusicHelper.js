/* ---------------------------------------------------------------
 *  音乐帮助类
 * ---------------------------------------------------------------
 */
let MusicHelper = cc.Class({
    ///播放音乐
    playMusic: function( path, name, is_loop, volume ) {
        if( is_loop === undefined ) {
            is_loop = false;
        }
        if( volume === undefined ) {
            volume = 1.0;
        }
        let self = this;
        let soundPool = null;
        if( name != undefined ) {
            soundPool = this.soundPool;
            if( !soundPool ) {
                soundPool = {};
                this.soundPool = soundPool;
            }
        }
        cc.loader.loadRes(path, cc.AudioClip, function (err, url) {
            let id = cc.audioEngine.play(url, is_loop, volume);
            if( name ) {
                soundPool[name] = id;
            }
        });
    },

    ///根据名字 停止 音乐
    stopMusicByName: function( name ) {
        if(!this.soundPool){
            return;
        }
        let id = this.soundPool[name];
        let state = cc.audioEngine.getState(id);
        if( state > 0 ) {
            cc.audioEngine.stop(id);
        }
    },

    ///停止所有的音乐
    stopAllMusic: function() {
        cc.audioEngine.stopAll();
    }
});


module.exports = MusicHelper;