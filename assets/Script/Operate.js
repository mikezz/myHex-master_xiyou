// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,
    properties: {
        //
        m_helpNode:cc.Node,
        //
        m_resultNode:cc.Node,
        //
        m_popMsgNode:cc.Node,
        //
        m_popScoreNode:cc.Node,
        //
        m_titleList:[cc.Node],
    },

    // LIFE-CYCLE CALLBACKS:
    onLoad () {
        window.Helper.music.stopAllMusic();
        window.Helper.music.playMusic('music/sound_bgm', 'bgm', true);
    },

    start () {

    },

    //帮助
    onBtnHelp(){
        cc.log('打开帮助界面...');
        this.m_helpNode.active = true;
        //this.showResult(10);
    },

    // update (dt) {},
    onBtnSkill( event, type ){
        if( type != 1 ){
            this.popMsg('功能尚未解锁....');
        } else {
            let self = this;
            let videoAd = wx.createRewardedVideoAd({
                adUnitId: 'adunit-852c7b1f9c97f8e8'
            });
            videoAd.load()
            .then(() => videoAd.show())
            .catch(err => {
                console.log(err.errMsg);
                self.popMsg('大师兄的广告拍完了,撩妹去了');
            });
            rewardedVideoAd.onClose(res => {
                // 用户点击了【关闭广告】按钮
                // 小于 2.1.0 的基础库版本，res 是一个 undefined
                if (res && res.isEnded || res === undefined) {
                    if( type == 1 ){
                        self.changeTitle();
                    }
                }
                else {
                    self.popMsg('观看完大师兄的广告他才会帮忙哦');
                }
            });
        }
    },

    changeTitle(){
        this.popMsg('猴子换桃!!');
        for(let i=0;i<this.m_titleList.length;i++){
            let node = this.m_titleList[i];
            let shape = node.getComponent('Shape');
            shape.resetTile();
        }
    },

    showResult( score ){
        let componentResult = this.m_resultNode.getComponent('componentResult');
        componentResult.showScore(score);
    },

    popMsg(msg){        
        this.m_popMsgNode.getComponent('componentPop').popMsg(msg);
    },

    popScore(score){
        this.m_popScoreNode.getComponent('componentPop').popMsg('/' + score);
    },

    btnGameAgain(){
        cc.director.loadScene('Game');
    }
});
